function generateTitleCase(elements) {
  let titleCaseString = "";
  for (let key in elements) {
    titleCaseString +=
      elements[key][0].toUpperCase() +
      elements[key].toLowerCase().substring(1) +
      " ";
  }
  return titleCaseString;
}


module.exports = generateTitleCase;


