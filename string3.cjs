function swapMonthAndDate(dateStr) {
  let splittedDate = dateStr.split("/");

  let date = splittedDate[0];
  let month = splittedDate[1];
  let year = splittedDate[2];

  return `${month}/${date}/${year}`;
}

module.exports = swapMonthAndDate;
