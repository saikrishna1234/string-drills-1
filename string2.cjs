function splitString(element) {
  let splittedArray = element.split(".");
  let numericArray = [];

  for (i = 0; i < splittedArray.length; i++) {
    if (/[a-zA-Z]/.test(splittedArray[i])) {
      return [];
    } else {
      numericArray.push(parseInt(splittedArray[i]));
    }
  }
  return numericArray;
}

module.exports = splitString;
