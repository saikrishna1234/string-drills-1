function convertToNumbers(element) {
  let str = element.replace("$", "");
  if (/[a-zA-Z]/.test(str)) {
    return 0;
  } else {
    let floatValue = parseFloat(str);
    return floatValue;
  }
}

module.exports = convertToNumbers;
