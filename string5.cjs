function generateString(elements) {
  if (elements.length > 0) {
    return elements.join(" ") + ".";
  } else {
    return "";
  }
}

module.exports = generateString;
